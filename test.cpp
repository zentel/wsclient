#include "wsclient.hpp"

#ifdef _WIN32
#pragma comment( lib, "ws2_32" )
#include <WinSock2.h>
#endif
#include <assert.h>
#include <stdio.h>
#include <string>

using wsclient::WebSocket;

class WebSocketCallback :public wsclient::WebSocketCallback
{
public:
	WebSocketCallback(wsclient::WebSocket*_ws)
		:ws(_ws)
	{
	}
	void OnMessage(const std::string& message) {
		printf("RX: %s\n",message.c_str());
		if (message == "world") 
			ws->close();
	}
	
	void OnMessage(const std::vector<uint8_t>& message) {
	}

	easywsclient::WebSocket* ws;
};

int main()
{
#ifdef _WIN32
    INT rc;
    WSADATA wsaData;

    rc = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (rc) {
        printf("WSAStartup Failed.\n");
        return 1;
    }
#endif

	WebSocketCallback callback;
    WebSocket::pointer ws = WebSocket::from_url("ws://localhost:8126/foo");
    assert(ws);
    ws->send("goodbye");
    ws->send("hello");
    while (ws->getReadyState() != WebSocket::CLOSED) {
      ws->poll();
      ws->dispatch(callback);
    }
    delete ws;
	
#ifdef _WIN32
    WSACleanup();
#endif
    return 0;
}
