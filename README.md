# wsclient

#### 项目介绍
websocket客户端实现，只有两个文件。

#### 安装教程

  将wsclient.hpp及wsclient.cpp拷贝到你的工程中使用即可


#### 使用说明

```c++
#include "wsclient.hpp"
//#include "wsclient.cpp" // <-- include only if you don't want compile separately

int
main()
{
    ...
    using wsclient::WebSocket;
    WebSocket::pointer ws = WebSocket::from_url("ws://localhost:8126/foo");
    assert(ws);
    while (true) {
        ws->poll();
        ws->send("hello");
        ws->dispatch(websocket_callback);
        // ...do more stuff...
    }
    ...
    delete ws; // alternatively, use unique_ptr<> if you have C++11
    return 0;
}
```

