workspace "wsclient"
	configurations { "Debug", "Release" }
	platforms { "Win32", "Win64", "Linux" }
	location "build"
	
filter { "platforms:Win32" }
    system "windows"
    architecture "x32"
	defines { "_WIN32", "WIN32","_CRT_SECURE_NO_WARNINGS"}
	
filter { "platforms:Win64" }
    system "windows"
    architecture "x64"	
	defines { "_WIN32", "WIN32" ,"_CRT_SECURE_NO_WARNINGS"}
	
filter { "platforms:Linux" }
    system "linux"
    architecture "x64"	
	defines { "LINUX", "linux" ,"POSIX"}
	
	
filter "configurations:Debug"
	defines { "DEBUG" , "_DEBUG"}
	symbols "On"
	optimize "Debug"
	
filter "configurations:Release"
	defines { "NDEBUG" }
	symbols "Off"
	optimize "Speed"
	vectorextensions "SSE2"
	
project "wsclient"
	kind "ConsoleApp"
	language "C++"
	
	includedirs{
		"./src",
	}
	files { 
		"*.h",
		"*.cpp",
	}

filter { "platforms:Win32" }
	links{
		"ws2_32.lib",
	}
	
filter { "platforms:Linux" }
	links{"pthread","rt"}

	
	
	
	